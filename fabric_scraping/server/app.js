var WebSocketServer = require('websocket').server;
var http = require('http');
var myrequest = require('request');
var fs = require('fs');

var server = http.createServer(function (request, response) { });
server.listen(12345, function () { });

wsServer = new WebSocketServer({ httpServer: server });

console.log('server listens on port 12345');

wsServer.on('request', function (request) {
    var connection = request.accept(null, request.origin);

    connection.on('message', function (message) {
        if (message.type === 'utf8') {

            var obj = JSON.parse(message.utf8Data);

            if (obj.url) {

                myrequest(obj.url, function (error, response, body) {
                    if (!error && response.statusCode == 200) {

                        obj = { html: body };
                        connection.send(JSON.stringify(obj));
                    }
                });
            }
            else if (obj.urls) {
              
                var obj = JSON.parse(message.utf8Data);
                var arr = obj.urls;

                arr.forEach(function (url) {

                    myrequest(url, function (error, response, body) {
                        if (!error && response.statusCode == 200) {

                            var arrayOfStrings = url.split('/');

                            var out = fs.createWriteStream(arrayOfStrings[arrayOfStrings.length - 1], { flags: 'w' });

                            out.write(body);

                            out.end(function () {
                                console.log('Finished writing to file: ' + arrayOfStrings[arrayOfStrings.length - 1]);
                            });
                        }
                    });
                });               
            }
        }
    });

    connection.on('close', function (connection) {
        // close user connection
    });
});