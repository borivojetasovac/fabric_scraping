(function () {
  
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    if (!window.WebSocket) {
        console.log('Sorry, but your browser doesn\'t support WebSockets.');
        return;
    }

    var connection = new WebSocket('ws://localhost:12345');

    connection.onopen = function () {
        console.log("uspostavljena veza sa node-om preko web socketa!");

        var obj = { url: "http://fabricjs.com/docs/" };
        connection.send(JSON.stringify(obj));
    };

    connection.onerror = function (error) {
        console.log('an error occurred when sending/receiving data');
    };

    connection.onmessage = function (message) {
       
        try {
            var obj = JSON.parse(message.data),
                html = $.parseHTML(obj.html);

            $("body").append(html);

            var arr = [];
            $("a").each(function (index) {
                var str = $(this).attr("href");
                var arrayOfStrings = str.split('/');

                //arrayOfStrings.forEach(function (item) {
                //    console.log(item);
                //});

                var flag = arrayOfStrings.some(function(item, index, array){
                    return (item === 'docs');
                });
                
                if (flag) {
                    arr.push("http://fabricjs.com" + str);
                }
            })
           
            arr.forEach(function (item) {
                console.log(item);
            });

            var obj = { urls: arr };
            connection.send(JSON.stringify(obj));
           
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ', message.data);
            return;
        }       
    };
})();